import { Component,  Input, Output, EventEmitter } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events  } from 'ionic-angular';
import { HomePage } from '../home/home';   
import { CountriesListPage } from '../countries-list/countries-list';
import { Utils } from '../../helpers/util';  
declare var window : any;  
import firebase from 'firebase';  


@IonicPage()
@Component({
  selector: 'page-phone-auth',
  templateUrl: 'phone-auth.html',
})
export class PhoneAuthPage {
 
  private enter_confirmation:boolean = false; 
  private enter_phone: boolean = false;  
  private enter_country: boolean = true; 
  private is_validated : boolean = false;  
  private country: any  = { code : '', phone : ''}  
  private language: string = ""; 
  private input_code : string = '';
  private input_phone : string = ''; 
  private validator_error : any = { phone: false, confirm_code : false  };   
  private modalCountryList; 
  


  constructor( public navCtrl: NavController, public navParams: NavParams, public modal : ModalController , private events : Events  ) { 
    //this.input_phone =  "+639177079916";  //hardcoded for now for testing     
    this.openCountryListModal();      
  }
 
  ionViewDidLoad() {  
  }

  private openCountryListModal() {
    this.modalCountryList = this.modal.create(CountriesListPage);
    this.modalCountryList.onDidDismiss((data)=>{ 
          if(data){
              this.country = data.country; 
              this.language = data.language; 
              this.enter_phone = true;  
          }
      });
    this.modalCountryList.present(); 
  } 
  
  

  private confirm_screen(){
    this.enter_phone        = false; 
    this.enter_confirmation = true;  
  }

  private authFirebase() : Promise<any> { 
    return new Promise<any>(resolve =>{  
        let newphone = this.country.phone + "" + this.input_phone;    
        newphone =  "+639177079916";  //hardcoded for now for testing     
        //newphone =  "+639452283010";  //hardcoded for now for testing      
    
        window.FirebasePlugin.verifyPhoneNumber(newphone, 60, (credential) => {      
                try{   
                      //check if user is already linked 
                      if( credential.code ) {  
                        resolve( { type : 'success', message : ''});
                      }else{

                        if(this.input_code != ""){  
                              let signInCredential  :any  = firebase.auth.PhoneAuthProvider.credential( credential.verificationId , this.input_code + "");      
                              if(signInCredential){  
                                firebase.auth().signInWithCredential(signInCredential).then((res : any) =>{ 
                                  if(res){ 
                                    resolve( { type : 'success', message : ''});
                                  }else{
                                    resolve( { type : 'invalid-code', message : ''});
                                  } 
                                }).catch((e) => {
                                  resolve( { type : 'invalid-code',message : e }); 
                                }); 


                              }else{ 
                                resolve( { type : 'invalid-code', message : ''});

                              }
                        }else{ 
                          resolve( { type : 'empty-code', message : ''});
                        } 
    
                      } 
              }catch(e){ 
                    resolve( { type : 'runtime-error-processing-code', message : e});
              }
    
          }, (e) => {  
              resolve( { type : 'error-verify-number', message : e });
          });  
    });
  }
                     

  

private sendSMSCode(){ 
  Utils.openLoader();  
  this.authFirebase().then((result:any) =>{
    if(result.type != 'success'){
      this.confirm_screen(); 
    }
  
    switch(result.type){  
      case 'success': 
         this.navCtrl.push(HomePage); 
      break; 
  
      case 'empty-code':
         //ignore 
      break; 

      case 'invalid-code': 
      case 'runtime-error-processing-code': 
      case 'error-verify-number':  
        Utils.alert("Error", result.message);
        console.error(result.message); 
      break;  
    }
  
    Utils.closeLoader();  
  }).catch(e=>{
    Utils.alert("Error", e);
    Utils.closeLoader();  
  }); 
} 
 
 
private clickConfirm(){
  this.sendSMSCode();
}
 
 
private onChangeNumber(){
  if(this.validator_error.phone == true){
    this.validator_error.phone  = (!Utils.isValidMobile(this.input_phone));  
  }
}

private clickStartGame(){  
         if(!Utils.isValidMobile(this.input_phone)){
            this.input_phone = '';  
            Utils.alert("Error","Invalid mobile phone"); 
            this.validator_error.phone = true;  
         }else{ 
            this.validator_error.phone = false;  
            this.sendSMSCode(); 
      }
  } 
}
