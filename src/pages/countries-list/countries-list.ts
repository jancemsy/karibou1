import { Component,  Input, Output, EventEmitter  } from '@angular/core';
import { IonicPage, NavController, NavParams , Events, ViewController } from 'ionic-angular';
import { ARABIC } from '../../helpers/localization/arabic'; 
import { ENGLISH } from '../../helpers/localization/english'; 


/**
 * Generated class for the CountriesListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-countries-list',
  templateUrl: 'countries-list.html',
})   
export class CountriesListPage { 

  //@Output()
  //countryChange : EventEmitter<any> = new EventEmitter<any>(); 
  
  private country_list:any[] = [];
  private language : string = "arabic"; 
     

  constructor(public navCtrl: NavController, public navParams: NavParams , private events : Events, private viewCtrl : ViewController ) {  
    this.initLanguage();
  } 
  
  private initLanguage(){ 
    let localization : any = this.language == 'arabic' ? ARABIC : ENGLISH;   
    this.country_list = []; 
    let country : string ; 
    for( let code of localization.codes ){ 
       country = localization.countries[code]; 
      if(country){ 
         this.country_list.push({ code : code.toLocaleLowerCase() , country: country, phone : localization.phones[code]  } ); 
      }
    } 
  }


  private clickSelect(item){   
      this.viewCtrl.dismiss( { country: item, language: this.language }  ); 
      //this.events.publish("input:country", item);  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CountriesListPage');
  }

}
