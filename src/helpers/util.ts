import {   AlertController,  ToastController, Alert, LoadingController  } from 'ionic-angular';
import { Injector,Injectable, Inject } from '@angular/core';  
import { LoadedModule } from 'ionic-angular/umd/util/module-loader';

@Injectable()
export class Utils{
  public static toast: ToastController;  
  public static alertcontroller : AlertController; 
  public static loadingCtrl : LoadingController;
  public static loading : any; 

    
  constructor(private toast: ToastController , private alertcontroller: AlertController , public loadingCtrl: LoadingController) { 
      Utils.toast =  toast; 
      Utils.alertcontroller = alertcontroller; 
      Utils.loadingCtrl = loadingCtrl;
 }
 
 
  public static loader() {
    Utils.presentLoadingDefault(); 
  }

  public static openLoader() {
    Utils.loading = Utils.loadingCtrl.create({
      content: 'Please wait...'
    }); 

    Utils.loading.present();
  }
  public static closeLoader() {
    Utils.loading.dismiss();
  }

 public static presentLoadingDefault() {
  let loading = Utils.loadingCtrl.create({
    content: 'Please wait...'
  });

  loading.present();

  setTimeout(() => {
    loading.dismiss();
  }, 5000);
}


 
 public static alert(title: string, message: string) { 
    let alert = Utils.alertcontroller.create({
        title: title,
        subTitle: message,
        buttons: ['OK']
    });

    alert.present();   
}


  public static isValidMobile(val): any { 
    let regExp = /^[0-9]{5,20}$/;

    if (!regExp.test(val)) {
        return false;
    }
    return true;
}
 
public static random(min, max){ 
       let a : any = Math.floor(Math.random() * (+max - +min)) + +min;  

       return (a).toLocaleString();
}

 public static  generateCode() : any {  
        return Utils.random(0,9) + "" + Utils.random(0,9) + "" +  Utils.random(0,9) + "" + Utils.random(0,9); 

 }
 
    
 public static async splash(message) {

   Utils.alert("Alert", message); 

  /*
    const toast = await Utils.toast.create({
      message: message,
      duration: 2000
    });
    toast.present();
    */
}



}