import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';



import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home'; 
import {PhoneAuthPage} from "../pages/phone-auth/phone-auth";
import {CountriesListPage} from "../pages/countries-list/countries-list";
import {Utils} from "../helpers/util";   
//import { Firebase  } from '@ionic-native/firebase/ngx'; 
//import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx'; 

//import { FirebaseModule  } from '@angular/fire/firebase.app.module'; 
import { AngularFireModule  } from '@angular/fire';   
//import { Firebase  } from '@ionic-native/firebase/ngx';  
 




const firebase_config = {
  apiKey: 'AIzaSyAmGVeLvZgxTNHlKLwvXqf-afRG93Fhz-8',
  authDomain: 'twlive-primary.firebaseapp.com',
  databaseURL: 'https://twlive-primary.firebaseio.com',
  projectId: 'twlive-primary',
  storageBucket: 'twlive-primary',
  messagingSenderId: '996062857326'
};

import * as firebase from 'firebase';

firebase.initializeApp(firebase_config);
 

@NgModule({
  declarations: [
    MyApp,
    HomePage, 
    PhoneAuthPage,
    CountriesListPage 
  ],  
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp), 
    AngularFireModule   //.initializeApp(firebase_config)   
  ],
  bootstrap: [IonicApp],
  entryComponents: [   
    MyApp, 
    PhoneAuthPage, 
    HomePage, 
    PhoneAuthPage,
    CountriesListPage   
  ],
  providers: [
    Utils, 
    //Firebase,  
    StatusBar,
    SplashScreen, 
    //FirebaseAuthentication, 
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
